import React, { useEffect } from 'react'

import { Routes, BrowserRouter, Route } from 'react-router-dom'
import { AuthProvider, useAuth } from '../hooks/auth'
import Logout from '../pages/Logout'
import Gallery from '../pages/Gallery'
import Home from '../pages/Home'
import Layout from './Layout'
import Login from '../pages/Login'
import UploadForm from '../pages/Upload'

const App = () => {

    return <BrowserRouter>
        <AuthProvider>
            <Layout>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/logout" element={<Logout />} />
                    <Route path="/gallery" element={<Gallery />} />
                    <Route path="/upload" element={<UploadForm />} />
                </Routes>
            </Layout>
        </AuthProvider>
    </BrowserRouter>
}

export default App