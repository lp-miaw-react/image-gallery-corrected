import React, { useEffect } from 'react'
import Navbar from './Navbar'

const Layout = props => {

    return <div className="h-full">
        <Navbar  />
        <main>
        <section className="absolute w-full h-full pt-16">
            <div
                className="absolute top-0 w-full bg-gray-900"
                style={{
                    // backgroundImage:
                    //   "url(" + require("assets/img/register_bg_2.png").default + ")",
                    backgroundSize: "100%",
                    backgroundRepeat: "no-repeat"
                }}
            ></div>
            {props.children}
            </section>
            {/* <FooterSmall absolute /> */}
        </main>
    </div>
}

export default Layout