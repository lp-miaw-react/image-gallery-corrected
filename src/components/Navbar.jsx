import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useAuth } from '../hooks/auth'

export default function Navbar(props) {
  const [authState] = useAuth()
  const [navbarOpen, setNavbarOpen] = useState(false)
  return (
    <>
      <nav
        className={
          ("top-0 absolute z-50 w-full bg-white shadow-lg") +
          " flex flex-wrap items-center justify-between px-2 py-3 "
        }
      >
        <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <Link
              className={
                (props.transparent ? "text-white" : "text-gray-800") +
                " text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase"
              }
              to="/"
            >
              LPMIAW Gallery
            </Link>
            <button
              className="cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
              type="button"
              onClick={() => setNavbarOpen(!navbarOpen)}
            >
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
              </svg>
            </button>
          </div>
          <div
            className={
              "lg:flex flex-grow items-center bg-white lg:bg-transparent lg:shadow-none" +
              (navbarOpen ? " block" : " hidden")
            }
            id="example-navbar-warning"
          >
            <ul className="flex flex-col lg:flex-row list-none mr-auto">
              <NavbarItem
                to="/"
                label="Accueil"
                onClick={() => setNavbarOpen(false)}
              />
              <NavbarItem
                to="/gallery"
                label="Galerie"
                onClick={() => setNavbarOpen(false)}
              />
            </ul>
            <ul className="flex flex-col lg:flex-row list-none lg:ml-auto">
              {!authState.user
                ? <NavbarItem
                  to="/login"
                  label="Connexion"
                  type="button"
                  onClick={() => setNavbarOpen(false)}
                />
                : <NavbarItem
                  to="/logout"
                  label="Déconnexion"
                  type="button"
                  onClick={() => setNavbarOpen(false)}
                />
              }
            </ul>
          </div>
        </div>
      </nav>
    </>
  )
}

const NavbarItem = props => {
  const {
    to,
    type,
    label,
    onClick
  } = props

  return <li className="flex items-center">
    <Link
      onClick={onClick}
      className={type ==='button'
        ? 'bg-pink-500 text-white active:bg-pink-600 text-xs font-bold uppercase px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none lg:mr-1 lg:mb-0 ml-3 mb-3'
        : 'text-gray-800 hover:text-gray-600 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold'
      }
      to={to}
    >
      {label}
    </Link>
  </li>
}