import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useAuth } from '../hooks/auth'
import Fab from '../components/Fab'
import Thumbnail from '../components/Thumbnail'

const Gallery = () => {

    const [images, setImages] = useState([])
    const [authState] = useAuth()

    useEffect(() => {
        fetch('/api/images')
        .then(res => {
            if(!res.ok){
                throw new Error('List retrieval failed')
            }
            return res.json()
        })
        .then(list => setImages(list))
    }, [])

    return  <div className="mx-auto">
        {authState.user && <Link to="/upload">
            <Fab icon="+" />
        </Link>}
        <ul className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6 gap-1 mx-auto">
            {images.map(img => <li key={img.id} className="">
                <Thumbnail
                    description={img.description}
                    url={img.url}
                />
            </li>)}
        </ul>
    </div>
}

export default Gallery