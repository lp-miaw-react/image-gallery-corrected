import React, { useEffect, useState } from 'react'
import { Navigate } from 'react-router'
import { useAuth } from '../hooks/auth'
import Alert from '../components/Alert'
import LoginForm from '../components/LoginForm'

// import FooterSmall from "components/FooterSmall.js";

export default function Login() {

  const [loginState, actions] = useAuth()

  const {
    login
  } = actions

  const {
    error,
    loading,
    user
  } = loginState
  
  const handleSubmit = ({
    username,
    password
  }) => {
    login(
      username,
      password
    )
  }

  // useEffect(() => {
  //   return () => {
  //     dispatch({
  //       type: 'login_reset'
  //     })
  //   }
  // }, [])

  return (
    <div className="container mx-auto px-4 h-full">
      {user && <Navigate to="/gallery" />}
      <div className="flex content-center items-center justify-center h-full">
        <div className="w-full lg:w-4/12 px-4">
          {/* Mettez l'alerte ici ! */}
        {error && <Alert
            status="error"
            description="Identifiants invalides"
        />}
          <LoginForm
            onSubmit={handleSubmit}
            disabled={loading}
          />
        </div>
      </div>
    </div>
  )
}
