import React from 'react'
import { Navigate } from 'react-router'
import { useAuth } from '../hooks/auth'

const Logout = () => {

    const [authState, dispatch] = useAuth()

    const handleLogout = () => {
        fetch('api/auth/logout', {
            method: 'POST',
            credentials: 'same-origin'
        })
        .then(res => {
            if(!res.ok){
                throw new Error('Failed to log out')
            }
            dispatch({
                type: 'logout_success'
            })
        })
    }

    return <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
            <div className="w-full lg:w-4/12 px-4 shadow py-24 text-center">
                {!authState.user && <Navigate to="/login" />}
                <p>Revenez vite !</p>
                <button className="mt-6 bg-pink-500 text-white active:bg-pink-600 text-xs font-bold uppercase px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none" onClick={handleLogout}>
                    Déconnexion
                </button>
            </div>
        </div>
    </div>
}

export default Logout
