import React, { useRef, useState } from 'react'
import { Navigate } from 'react-router'
import { useAuth } from '../hooks/auth'
import Result from '../components/Result'

const UploadForm = props => {

    const [loginState] = useAuth()
    const [uploadState, setUploadState] = useState(null)
    const [formState, setFormState] = useState({
        description: '',
        private: true
    })

    const [imageUrl, setImageUrl] = useState(null)

    const fileInputRef = useRef(null)

    const handleFormChange = evt => {
        setFormState({
            ...formState,
            [evt.target.name]: evt.target.type === 'checkbox'
                ? evt.target.checked
                : evt.target.value
        })
    }

    const handleFileInputChange = evt => {
        const file = evt.target.files?.[0]
        if(file){
            setImageUrl(
                URL.createObjectURL(file)
            )
        }
    }

    const handleSubmit = e => {
        e.preventDefault()
        const file = fileInputRef.current.files?.[0]
        const formData = new FormData()

        for(const key in formState){
            formData.append(key, formState[key])
        }

        formData.append('image', file)

        fetch('/api/images', {
            method: 'POST',
            body: formData
        })
        .then(res => res.json())
        .then(() => {
            setUploadState('success')
        })
        .catch(() => {
            setUploadState('error')
        })

    }

    return <div className="container mx-auto px-4 h-full">
        {!loginState.user && <Navigate to="/login" state={{from: '/upload'}} />}
        <div className="flex content-center items-center justify-center h-full">
            <div className="w-full lg:w-4/12 px-4">
                <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                    <div className="flex-auto px-4 lg:px-10 py-10 pt-4">
                        {uploadState
                        ? <Result status={uploadState} />
                        : <form onSubmit={handleSubmit}>
                            <div className="relative w-full mb-3">
                                <label
                                    className="block uppercase text-gray-700 text-xs font-bold mb-2"
                                    htmlFor="image"
                                >
                                    Image
                                </label>
                                <input
                                    type="file"
                                    name="image"
                                    id="image"
                                    accept="image/*"
                                    className="hidden"
                                    ref={fileInputRef}
                                    onChange={handleFileInputChange}
                                />
                                <button
                                    type="button"
                                    onClick={() => fileInputRef.current.click()}
                                    className="w-full px-3 py-3 bg-white rounded shadow text-4xl text-gray-400 flex flex-col items-center justify-center"
                                >
                                    {imageUrl
                                        ? <img src={imageUrl} alt="Uploaded" className="object-cover w-full" />
                                        : <>
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-12 w-12" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12" />
                                            </svg>
                                            <p className="text-xl text-gray-700">
                                                Choisir un fichier
                                            </p>
                                        </>
                                    }
                                </button>
                            </div>
                            <div className="relative w-full mb-3">
                                <label
                                    className="block uppercase text-gray-700 text-xs font-bold mb-2"
                                    htmlFor="description"
                                >
                                    Description
                                </label>
                                <textarea
                                    id="description"
                                    className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                    name="description" id="description" cols="30" rows="5"
                                    onChange={handleFormChange}
                                    value={formState.description}
                                >
                                </textarea>
                            </div>

                            <div className="text-center mt-6">
                                <input type="submit"
                                    className="bg-gray-900 text-white disabled:bg-gray-400 active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                                    style={{ transition: "all .15s ease" }}
                                    disabled={loginState.loading || !fileInputRef.current?.files?.[0]}
                                    value="Envoyer"
                                />
                            </div>
                        </form>}
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default UploadForm
