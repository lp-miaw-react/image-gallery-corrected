import React from 'react'
import { render, screen } from '@testing-library/react'
import Alert from '../components/Alert'

describe('Alert', () => {
    test('should render without props', () => {
        render(<Alert />)
    })

    test('should display its `title` and `description` props', () => {
        const testTitle = "123Title"
        const testDescription = "123Description"
        render(<Alert title={testTitle} description={testDescription} />)

        expect(screen.getByText(testTitle)).toBeInTheDocument()
        expect(screen.getByText(testDescription)).toBeInTheDocument()
    })
})