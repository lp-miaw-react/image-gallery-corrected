import React from 'react'

import { screen, render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { AuthContext, AuthProvider } from '../hooks/auth'
import Login from '../pages/Login'

const originalFetch = fetch


describe('Login', () => {

    afterEach(() => {
        global.fetch = originalFetch
    })

    test('should render within AuthContext', async () => {
        global.fetch = jest.fn(() => Promise.resolve({
            json: () => Promise.resolve({})
        }))
        render(<AuthProvider>
            <Login />
        </AuthProvider>
        )

        userEvent.type(screen.getByLabelText('Email'), 'test@mail.com')
        userEvent.type(screen.getByLabelText('Mot de passe'), 'azerty')
        
        userEvent.click(screen.getByRole('button'))

        await waitFor(() => expect(global.fetch).toHaveBeenCalledWith('/api/auth/login', {"headers": {"Authorization": "Basic dGVzdEBtYWlsLmNvbTphemVydHk="}, "method": "POST"})) 
        
    })
})